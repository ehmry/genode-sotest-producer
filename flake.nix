{
  description = "A Sotest producer for Genode";

  edition = 201909;

  outputs = { self, genodepkgs }: {
    defaultPackage.x86_64-linux =
      genodepkgs.packages.x86_64-linux-x86_64-genode.sotest-producer;
  };
}
